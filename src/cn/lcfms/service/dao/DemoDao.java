package cn.lcfms.service.dao;

import cn.lcfms.bean.Demo;

public interface DemoDao {	
	public Demo getDemo(int id);	
}
