package cn.lcfms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.lcfms.bean.Demo;
import cn.lcfms.service.dao.DemoDao;
import cn.lcfms.unit.Vardump;

@Service
public class DemoService {
	@Autowired
	private DemoDao dao;
	public void getDemo(){
		Demo demo = dao.getDemo(1);
		Vardump.print(demo);
	}
}
