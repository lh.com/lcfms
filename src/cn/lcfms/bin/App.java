package cn.lcfms.bin;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import cn.lcfms.service.BaseService;


public class App implements ApplicationContextAware{
	public static SqlSessionFactory factory=null;
	public static String ROOT_PATH="";//系统根目录
	public static String CONF_PATH="";//系统配置目录
	public static List<HashMap<String, Object>> ITEM;//导航列表
	public static HashMap<String, Object> APPCONFIG;
    private String url;
    private String tablePrefix;
    
    public void init() throws Exception{
    	System.out.println("初始化系统");
    	//设置service
    	BaseService.init(url,tablePrefix);
    	//设置根目录
    	if(ROOT_PATH.equals("")){
    		ROOT_PATH = System.getProperty("evan.webapp");
    		CONF_PATH = ROOT_PATH+"WEB-INF"+File.separator+"classes"+File.separator+"config"+File.separator;
    	}
    	if(APPCONFIG==null){
    		APPCONFIG=new HashMap<String, Object>();
    		ResourceBundle config=ResourceBundle.getBundle("config.lcfms");
    		Set<String> keySet = config.keySet();
    		Iterator<String> iterator = keySet.iterator();
    		while(iterator.hasNext()){
    			String next = iterator.next();
    			String keyValue = new String(config.getString(next).getBytes("ISO-8859-1"), "utf-8");
    			if(keyValue.equals("true") || keyValue.equals("false")){
    				APPCONFIG.put(next, Boolean.valueOf(keyValue));
    				continue;
    			}
    			Pattern r1 = Pattern.compile("\\d+");	
    			Matcher m1 = r1.matcher(keyValue);
    			if(m1.matches()){
    				APPCONFIG.put(next, Integer.valueOf(keyValue));
    				continue;
    			} 
    			Pattern r2 = Pattern.compile("\\d+\\.\\d");	
    			Matcher m2 = r2.matcher(keyValue);
    			if(m2.matches()){
    				APPCONFIG.put(next, Float.valueOf(keyValue));
    				continue;
    			} 
    			if(keyValue.equals("null")){
    				APPCONFIG.put(next, null);
    				continue;
    			}
    			APPCONFIG.put(next, keyValue);			
    		}
    	}
    }

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public static SqlSession getSession(){		
		SqlSession session = factory.openSession(true);		
		return session;
	}
	
	public static BaseService getService(String table){			
		BaseService service=new BaseService(table);	
		return service;
	}
	public static BaseService getService(){			
		BaseService service=new BaseService();	
		return service;
	}
	
    @Override
	public void setApplicationContext(ApplicationContext ctx)throws BeansException {
    	factory=(SqlSessionFactory) ctx.getBean("sqlSessionFactory");	
		Configuration configuration = factory.getConfiguration();
		try {
			configuration.addMapper(Class.forName("cn.lcfms.service.dao.BaseDao"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	
    }
      
    public static void printEnd(HttpServletRequest request){
		Userinfo.putUserInfo("TIMEEND", new Date().getTime(), request);
		Userinfo.putUserInfo("MEMORYEND", Runtime.getRuntime().freeMemory(), request);
    	System.out.println("系统执行结束，完成时间："+((long)Userinfo.getUserInfo("TIMEEND", request)-(long)Userinfo.getUserInfo("TIMESTART", request))+"毫秒"+"，使用内存："+((long)Userinfo.getUserInfo("MEMORYSTART", request)-(long)Userinfo.getUserInfo("MEMORYEND", request))/1024+"kb");
		System.out.println("");
		System.out.println("");
    }
}
