package cn.lcfms.bin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Userinfo {
	@SuppressWarnings("unchecked")
	public static Object getUserInfo(String key,HttpServletRequest request){
		HashMap<String, Object> map=(HashMap<String, Object>) request.getAttribute("USERINFO");
		return map.get(key);
	}
	@SuppressWarnings("unchecked")
	public static void putUserInfo(String key,Object value,HttpServletRequest request){
		try {
			HashMap<String, Object> map=(HashMap<String, Object>) request.getAttribute("USERINFO");
			map.put(key, value);
		} catch (Exception e) {
			HashMap<String, Object> map=new HashMap<>();
			map.put(key, value);
			request.setAttribute("USERINFO", map);
		}		
	}
	/**
	 * 设置cookies
	 * @param action
	 * @param key 键
	 * @param value 值
	 * @param time 时间
	 * @throws UnsupportedEncodingException
	 */
	public static void setCookies(String key,String value,int time,HttpServletResponse response) throws UnsupportedEncodingException{
		value=URLEncoder.encode(value,"utf-8");
		Cookie cook=new Cookie(key, value);
		cook.setMaxAge(time);		 
		cook.setPath("/");
        response.addCookie(cook);
	}
	/**
	 * 获取单条cookies
	 * @param action
	 * @param key
	 * @return
	 */
	public static String getCookies(String key,HttpServletRequest request){
		Cookie[] cookies = request.getCookies();
		String result="";		
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if(cookie.getName().equals(key)){
					try {
						result=URLDecoder.decode(cookie.getValue(),"utf-8");
					} catch (UnsupportedEncodingException e) {
						result=cookie.getValue();
					}
					break;
				}
				
			}
		}
		return result;
	}
}
