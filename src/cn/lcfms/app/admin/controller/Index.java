package cn.lcfms.app.admin.controller;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import cn.lcfms.bin.PermitPoll;
import cn.lcfms.bin.Userinfo;

@PermitPoll(type="系统管理",name="后台框架",forward="login.html")
@Controller("admin.Index")
@RequestMapping("/admin/index")
public class Index extends BaseController{	
		
	@RequestMapping("/easyui")
	public ModelAndView init(HttpServletRequest request){
		ModelAndView view=new ModelAndView("admin/easyui");			
		int gid=(int) Userinfo.getUserInfo("gid", request);
		boolean loop = true;
		List<HashMap<String, Object>> list=BaseController.itemcache;
		a:for (int i = 0; i < list.size(); i++) {
			if (!loop) {
				break a;
			}
			if ((int) list.get(i).get("parentId") == 1) {				
				String pstr=(String) list.get(i).get("permitId");
				if(null!=pstr && !"".equals(pstr)){
					String[] split = pstr.split(",");
					b:for(int j=0;j<split.length;j++){
						if(Integer.valueOf(split[j])==gid){
							request.setAttribute("TOPMENUID", list.get(i).get("itemId"));
							request.setAttribute("TOPMENUNAME", list.get(i).get("itemName"));
							request.setAttribute("TOPMENUICON", list.get(i).get("icon"));
							loop = false;
							break b;
						}
					}
				}
			}
		}
		return view;
	}
	
	@RequestMapping("/ace")
	public ModelAndView ace(HttpServletRequest request){
		ModelAndView view=new ModelAndView("admin/ace");	
		return view;
	}
	
	@RequestMapping("/first")
	public ModelAndView first(HttpServletRequest request){
		ModelAndView view=new ModelAndView("admin/first");					
		return view;
	}	
}
