<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>欢迎使用老成FMS (5.0正式版)-java web框架</title>
		<meta name="keywords" content="java快速开发框架,java编程,java web"> 
	    <meta name="description" content="老成fms(Frame Management System)，是一款前端，后台，业务集成的快速开发框架">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
		<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
		<link rel="stylesheet" href="${CSS}b.tabs.css" />		
		<link rel="stylesheet" href="${CSS}fonts.googleapis.com.css" />
		<link rel="stylesheet" href="${CSS}ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="${CSS}ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="${CSS}ace-skins.min.css" />
		<link rel="stylesheet" href="${CSS}ace-rtl.min.css" />
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="${CSS}ace-ie.min.css" />
		<![endif]-->		
		<!--[if !IE]> -->
		<script src="${JS}jquery-2.1.4.min.js"></script>
		<!-- <![endif]-->	
		<!--[if IE]>
		<script src="${JS}jquery-1.11.3.min.js"></script>
		<![endif]-->	
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='${JS}jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>	
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}b.tabs.min.js"></script>
		<script src="${JS}ace-elements.min.js"></script>
		<script src="${JS}ace.min.js"></script>
		<script src="${JS}ace-extra.min.js"></script>
		<!--[if lte IE 8]>
		<script src="${JS}html5shiv.min.js"></script>
		<script src="${JS}respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="index.html" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							老成FMS (5.0正式版)
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right hidden-xs" role="navigation">					
					<ul class="nav ace-nav">
						<li class="green dropdown-modal">
							<a class="dropdown-toggle" href="${APP}index/tag/init" target="_blank">
								UI在线编辑器
							</a>
						</li>

						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="${IMG}avatars/user.jpg"/>
								<span class="user-info">
									<small>欢迎你,</small>
									${USERINFO.aname}
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-cog"></i>
										系统设置
									</a>
								</li>
								<li>
									<a href="profile.html">
										<i class="ace-icon fa fa-user"></i>
										个人信息
									</a>
								</li>
								<li>
									<a href="#">
										<i class="ace-icon fa fa-power-off"></i>
										退出登录
									</a>
								</li>
							</ul>
						
						</li>
					</ul>
				
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="ace-icon fa fa-signal"></i>
							</button>
	
							<button class="btn btn-info">
								<i class="ace-icon fa fa-pencil"></i>
							</button>
	
							<button class="btn btn-warning">
								<i class="ace-icon fa fa-users"></i>
							</button>
	
							<button class="btn btn-danger">
								<i class="ace-icon fa fa-cogs"></i>
							</button>
						</div>
	
						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>
	
							<span class="btn btn-info"></span>
	
							<span class="btn btn-warning"></span>
	
							<span class="btn btn-danger"></span>
						</div>
				</div>
				<jsp:include page="left.jsp"/>
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="span10" id="mainFrameTabs">
					    <ul class="nav nav-tabs" role="tablist">
					        <li role="presentation" class="active noclose"><a href="#bTabs_186" data-toggle="tab">控制台</a></li>
					    </ul>				     
					    <div class="tab-content" style="height: 95%;">
					        <div class="tab-pane active" id="bTabs_186">
					            <iframe id="iframebTabs_186"  style="width:100%;height:100%;border:0px;overflow:scroll;" src="${APP}admin/index/first"></iframe>
					        </div>
					    </div>				 
					</div>
					<script>
						    (function(){		    	
						    	$('#mainFrameTabs').bTabs({
									sortable:true,
								    'resize' : function(){							    	
								        $('#mainFrameTabs').css("margin-top","5px");
								        $('#mainFrameTabs>.tab-content').css("border","0");
								        $('#mainFrameTabs>.tab-content').css("padding","10px");
								    }							    
								});
								$('a',$('#leftItem')).on('click', function(e) {
									var m=$(this).hasClass("dropdown-toggle");
									var itemId=$(this).attr("itemId");
									var title=$(this).attr("title");
									var url=$(this).attr("url");
									if(!m){							 
										$('#mainFrameTabs').bTabsAdd(itemId,title,"${APP}"+url);	
										$('#mainFrameTabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
										   showLeftCheck();
										})
										showLeftCheck();																									
									}
								});
								showLeftCheck();
								function showLeftCheck(){
									var active=$("#mainFrameTabs>ul>li[class*='active']");
									var href=$($(active).children("a")).attr("href");
									href=href.substr(7);	
									if(!isNaN(href)){
										 	$("#leftItem li").removeClass("active");
											$("#leftItem li").removeClass("open");									
											$("#leftItemId_"+href).addClass("active");
											$("#leftItemId_"+href).parents("li").addClass("open active");	
									}														
								}								

								//检测窗口高度
								setInterval(function(){								
									var active=$("#mainFrameTabs>ul>li[class*='active']");
									var href=$($(active).children("a")).attr("href");
									href=href.substr(7);								
									if(!isNaN(href)){
										var height=$($('#iframebTabs_'+href)[0].contentWindow.document.body).height();
										if(height<500){
											height=screen.height-215;
										}else{
											height=height+80;
										}	
										$('#mainFrameTabs').height(height);	
									}																			
								},50)
						    })()						
					</script>				
				</div>
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->	
	</body>
</html>
