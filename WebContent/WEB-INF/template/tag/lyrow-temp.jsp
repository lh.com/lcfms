<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
</head>
<body>
<div class="container-fluid" id="appendHtml">
<!--js_begin-->
<div class="row">
<c:forEach var="val" items="${lyrow}">
<div class="col-xs-${val}" style="background:#9fc6e7;">${val}0</div>
</c:forEach>
</div>
<!--js_end-->
</div>
</body>
</html>
 