<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
<div class="row">
    <a href="javascript:modal();" style="position: relative;top: 20px;left: 20px;">触发模态窗口</a>
	<div class="modal fade" id="myModal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">     
	       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>       
	        <h4 class="modal-title">模态窗口标题</h4>
	      </div>
	      <div class="modal-body">内容</div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
	        <button type="button" class="btn btn-primary" onclick="close();">保存</button>
	      </div>
	    </div>
	  </div>
	  <script>
	  //打开
	  function modal(){
		  $('#myModal').modal('toggle');
	  }	
	  //关闭
	  function close(){
		  $('#myModal').modal('hide');
	  }	
	  $(function(){
		  $('#myModal').on('show.bs.modal', function (e) {
			   alert("打开时调用。。。");
		  });
		  
		  $('#myModal').on('hidden.bs.modal', function (e) {
			   alert("关闭时调用。。。");
		  });
	  });	    
	  </script>
	</div>									
  </div>
</div>	      
</body>
</html>             