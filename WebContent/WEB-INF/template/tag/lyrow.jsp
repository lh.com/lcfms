<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">自定义栅格：</label>
	<div class="controls">
		<input type="text" name="setsg" placeholder="示例：30:30:30:30，和必须为120" value="${lyrow}" style="width:70%;"/> 
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">背景色：</label>
	<div class="controls">
	<select id="simple-colorpicker-1" name="backcolor" class="hide">	
        <option value="#fff">#fff</option>	
		<option value="#d06b64">#d06b64</option>
		<option value="#f83a22">#f83a22</option>
		<option value="#fa573c">#fa573c</option>
		<option value="#ff7537">#ff7537</option>
		<option value="#ffad46">#ffad46</option>
		<option value="#42d692">#42d692</option>
		<option value="#16a765">#16a765</option>
		<option value="#7bd148">#7bd148</option>
		<option value="#b3dc6c">#b3dc6c</option>
		<option value="#fbe983">#fbe983</option>
		<option value="#fad165">#fad165</option>
		<option value="#92e1c0">#92e1c0</option>
		<option value="#9fe1e7">#9fe1e7</option>
		<option value="#9fc6e7" selected>#9fc6e7</option>
		<option value="#4986e7">#4986e7</option>
		<option value="#9a9cff">#9a9cff</option>
		<option value="#b99aff">#b99aff</option>
		<option value="#c2c2c2">#c2c2c2</option>
		<option value="#cabdbf">#cabdbf</option>
		<option value="#cca6ac">#cca6ac</option>
		<option value="#f691b2">#f691b2</option>
		<option value="#cd74e6">#cd74e6</option>
		<option value="#a47ae2">#a47ae2</option>
		<option value="#555">#555</option>
	</select>
	</div>	
</div>
<div class="form-group">
	<label class="control-label" type="text">边框色：</label>
	<div class="controls">
	<select id="simple-colorpicker-2" name="bordercolor" class="hide">	
        <option value="#DDDDDD" selected>#DDDDDD</option>	
		<option value="#d06b64">#d06b64</option>
		<option value="#f83a22">#f83a22</option>
		<option value="#fa573c">#fa573c</option>
		<option value="#ff7537">#ff7537</option>
		<option value="#ffad46">#ffad46</option>
		<option value="#42d692">#42d692</option>
		<option value="#16a765">#16a765</option>
		<option value="#7bd148">#7bd148</option>
		<option value="#b3dc6c">#b3dc6c</option>
		<option value="#fbe983">#fbe983</option>
		<option value="#fad165">#fad165</option>
		<option value="#92e1c0">#92e1c0</option>
		<option value="#9fe1e7">#9fe1e7</option>
		<option value="#9fc6e7">#9fc6e7</option>
		<option value="#4986e7">#4986e7</option>
		<option value="#9a9cff">#9a9cff</option>
		<option value="#b99aff">#b99aff</option>
		<option value="#c2c2c2">#c2c2c2</option>
		<option value="#cabdbf">#cabdbf</option>
		<option value="#cca6ac">#cca6ac</option>
		<option value="#f691b2">#f691b2</option>
		<option value="#cd74e6">#cd74e6</option>
		<option value="#a47ae2">#a47ae2</option>
		<option value="#555">#555</option>
	</select>
	</div>	
</div>
<div class="form-group">
	<label class="control-label" type="text">边框：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="useborder" type="radio"  class="ace" value="1"/>
			<span class="lbl"> 有边框</span>
		</label>
		<label>
			<input name="useborder" type="radio"  class="ace" value="0" checked/>
			<span class="lbl"> 无边框</span>
		</label>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="control-label" type="text">内边距：</label>
	<div class="controls">
		<input type="text" name="padding" placeholder="内边距" value="0 15px"/> 
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">堆叠：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="duistyle" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 永久水平</span>
		</label>
		<label>
			<input name="duistyle" type="radio"  class="ace" value="2"/>
			<span class="lbl"> 750px</span>
		</label>
		<label>
			<input name="duistyle" type="radio"  class="ace" value="3"/>
			<span class="lbl"> 970px</span>
		</label>
		<label>
			<input name="duistyle" type="radio"  class="ace" value="4"/>
			<span class="lbl"> 1170px</span>
		</label>		
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="useStyle" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 自适宽度</span>
		</label>
		<label>
			<input name="useStyle" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 固定宽度</span>
		</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
	    $('#simple-colorpicker-1').ace_colorpicker();				
		$('#simple-colorpicker-2').ace_colorpicker();	
    });
	function saveEdit(){
	   var modalElement=getEditHtml();
	   var setsg=document.Editform.setsg.value;
	   var duistyle=$("input[name=duistyle]:checked").val();
	   var sg=setsg.split(":");
	   var m=0;
	   var h="";
	   for(var i=0;i<sg.length;i++){		   
		  var s=parseInt(sg[i]);
		  if(s!=10 && s!=20 && s!=30 && s!=40 && s!=50 && s!=60 && s!=70 && s!=80 && s!=90 && s!=100 && s!=110 && s!=120){
			  m=1000;
		  }else{
			  if(duistyle==1){
				  h+="<div class=\"col-xs-"+sg[i]/10+"\" style=\"background:#9fc6e7;\">"+sg[i]+"</div>";
			  }else if(duistyle==2){
				  h+="<div class=\"col-sm-"+sg[i]/10+"\" style=\"background:#9fc6e7;\">"+sg[i]+"</div>";
			  }else if(duistyle==3){
				  h+="<div class=\"col-md-"+sg[i]/10+"\" style=\"background:#9fc6e7;\">"+sg[i]+"</div>";
			  }else if(duistyle==4){
				  h+="<div class=\"col-lg-"+sg[i]/10+"\" style=\"background:#9fc6e7;\">"+sg[i]+"</div>";
			  }		
			  m+=s; 
		  }	
	   }
	   if(m==120){
		   modalElement.children().find(".row:last").html(h);
	   }	   	   	   
	   var backcolor=document.Editform.backcolor.value;
	   modalElement.children().find(".row:last>div").css("background-color",backcolor);	   
	   var useborder=$("input[name=useborder]:checked").val();
	   if(useborder==0){
	        modalElement.children().find(".row:last>div").css("border","0");			
	   }else{
		   var bordercolor=document.Editform.bordercolor.value;
		   modalElement.children().find(".row:last>div").css("border","1px solid "+bordercolor);
	   }	   
	   var padding=document.Editform.padding.value;
	   modalElement.children().find(".row:last>div").css("padding",padding);
	   var useStyle=$("input[name=useStyle]:checked").val();
	   if(useStyle==1){
		   modalElement.children(".container").attr("class","container-fluid");
	   }else{
		   modalElement.children(".container-fluid").attr("class","container");
	   }
	}
</script>
</body>
</html>
