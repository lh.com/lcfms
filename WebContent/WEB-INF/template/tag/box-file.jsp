<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<link rel="stylesheet" href="${CSS}chosen.min.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script type="text/javascript" src="${JS}chosen.jquery.min.js"></script> 
	<script src="${JS}showtag.js"></script>
	<script>
		jQuery.fn.extend({
			dropzone:function(){}
		});
	</script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">标题：</label>
	<div class="controls">
		<input placeholder="标题" type="text" name="title" value="上传文件"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">文件小于（MB）：</label>
	<div class="controls">
		<div class="clearfix">
			<input class="col-xs-4" type="text" name="size" placeholder="1" value="1"/> 
		</div>		
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">文件数目：</label>
	<div class="controls">
		<div class="clearfix">
			<input class="col-xs-4" type="text" name="number" placeholder="1" value="1"/> 
		</div>		
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">文件类型：</label>
	<div class="controls">
		<div class="clearfix">
			<input class="col-xs-8" type="text" name="type" placeholder="image/*" value="image/*"/> 
		</div>
        使用逗号分隔的MIME type或者扩展名，例如：image/*,application/pdf,.psd,.js		
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">背景文字：</label>
	<div class="controls">
		<div class="clearfix">
			<input class="col-xs-8" type="text" name="font" placeholder="点击或拖拽单张图片" value="点击或拖拽单张图片"/> 
		</div>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">url：</label>
	<div class="controls">
		<div class="clearfix">
			<input class="col-xs-8" type="text" name="url" placeholder="imgFile" value="uploadfile"/> 
		</div>
	该url为请求到服务器并保存文件的地址	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">背景图片：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="image" type="radio"  class="ace" value="fa fa-picture-o" checked /> 
			<span class="lbl"> <i class="fa fa-picture-o bigger-160 blue"></i></span>
		</label>
		<label>
			<input name="image" type="radio"  class="ace" value="fa fa-cloud-upload"/> 
			<span class="lbl"> <i class="fa fa-cloud-upload bigger-160 blue"></i></span>
		</label>
		<label>
			<input name="image" type="radio"  class="ace" value="fa fa-upload"/> 
			<span class="lbl"> <i class="fa fa-upload bigger-160 blue"></i></span>
		</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  	
	function saveEdit(){
		var modalElement=getEditHtml();
		var title=document.Editform.title.value;
		modalElement.children().find(".control-label:last").html(title+"：");	
	    var size=document.Editform.size.value;
	    var number=document.Editform.number.value;
	    var type=document.Editform.type.value;
	    var font=document.Editform.font.value;
	    var url=document.Editform.url.value;
		var image=$("input[name=image]:checked").val();     
		modalElement.children().find(".dropzone:last .file-text").html("<i class=\"fa fa-caret-right red\"></i>"+font);
		modalElement.children().find(".dropzone:last .file-name").html("<i class=\""+image+"\"></i>");
		modalElement.children().find(".dropzone:last").attr("maxfilesize",size);
		modalElement.children().find(".dropzone:last").attr("acceptedFiles",type);
		modalElement.children().find(".dropzone:last").attr("acceptedFiles",type);
		modalElement.children().find(".dropzone:last").attr("url",url);
		modalElement.children().find(".dropzone:last").attr("maxFiles",number);
		getEditWindow().form.init("myform");	
	}	
</script>
</body>
</html>

