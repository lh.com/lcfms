<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">value值：</label>
	<div class="controls">
		<input type="text" value="1" name="val"/>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">最小值：</label>
	<div class="controls">
		<input type="text" value="1" name="min"/>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">最大值：</label>
	<div class="controls">
		<input type="text" value="10" name="max"/>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">step值：</label>
	<div class="controls">
		<input type="text" value="1" name="step"/>	
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  	
	function saveEdit(){
		var modalElement=getEditHtml();
	    var min=document.Editform.min.value;
	    var max=document.Editform.max.value;
	    var step=document.Editform.step.value;	    
	    var val=document.Editform.val.value;	
	    var str="<input type=\"text\" readonly value=\""+val+"\" class=\"spinner-input\" min-spinner=\""+min+"\" max-spinner=\""+max+"\" step-spinner=\""+step+"\">";
		modalElement.children().find(".row").html(str);
		getEditWindow().spinner.init();	
	}	
</script>
</body>
</html>

