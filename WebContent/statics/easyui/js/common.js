(function($) {
	var ws;
	var target;
	window.socket = {
		connect : function(obj) {
			var userName=obj.userName;
			var userId=obj.userId;
			var id=obj.id;
			var url=obj.url.substr(7,obj.url.length-1);
			target ="ws://"+url+"/websocket/getSocketConnct?userName="+userName+"&userId="+userId+"&id="+id;
			if ('WebSocket' in window) {
				ws = new WebSocket(target);
				if (obj.success) {
					obj.success();
				}
			} else if ('MozWebSocket' in window) {
				ws = new MozWebSocket(target);
				if (obj.success) {
					obj.success();
				}
			} else {
				alert("您的浏览器不支持websocket！");
				return;
			}
			ws.onmessage = function(event) {
				obj.message(event.data);
			};
		},
		send : function(msg) {
			ws.send(msg);
		},
		close : function() {
			ws.close();
		}
	}
})($);
(function() {
	window.common = {
		getCookie : function(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';'); //把cookie分割成组    
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i]; //取得字符串    
				while (c.charAt(0) == ' ') { //判断一下字符串有没有前导空格    
					c = c.substring(1, c.length); //有的话，从第二位开始取    
				}
				if (c.indexOf(nameEQ) == 0) { //如果含有我们要的name    
					return unescape(c.substring(nameEQ.length, c.length)); //解码并截取我们要值    
				}
			}
			return false;
		},
		//清除cookie    
		clearCookie : function(name) {
			setCookie(name, "", -1);
		},
		//设置cookie   
		setCookie : function(name, value, seconds) {
			seconds = seconds || 0; //seconds有值就直接赋值，没有为0，这个根php不一样。    
			var expires = "";
			if (seconds != 0) { //设置cookie生存时间    
				var date = new Date();
				date.setTime(date.getTime() + (seconds * 1000));
				expires = "; expires=" + date.toGMTString();
			}
			document.cookie = name + "=" + escape(value) + expires + "; path=/"; //转码并赋值    
		},
		//去空格
		trim : function(str) {
			var str = str.replace(/^\s*/, "");
			var str = str.replace(/\s*$/, "");
			return str;
		},
		showpic:function(id){
			easyui.openwindow({
				href:'/system/store/showpic?dataId='+id,
				title:'查看图片',
				width:400,
				height:500,
				iconCls:'icon-pencil'
			});
		}
	}
})();
(function($) {
	window.easyui = {
		openwindow : function(obj) {
			if(obj==null || obj=="" || obj=='undefind'){
				var obj={};
			}else if(Object.prototype.toString.call(obj) === "[object String]"){
				var obj={href:obj};
			}
			obj.title=obj.title?obj.title:"新窗口";
			obj.modal=obj.modal?obj.modal:true;
			obj.iconCls=obj.iconCls?obj.iconCls:"icon-edit";	
			obj.width=obj.width?obj.width:500;	
			obj.height=obj.height?obj.height:400;	
			$("#newWindow").css("padding",'5px');
			$("#newWindow").css("z-index",'9999');
			$("#newWindow").window(obj);
		},
		closewindow:function(obj){
			$("#newWindow").window("close");
		},
		addTab:function(title, url){
			var maintabs;
			if($('#maintabs').length>0){
				maintabs=$('#maintabs');				
			}else{
				maintabs=parent.$('#maintabs');	
			}
			if (maintabs.tabs('exists', title)){
				maintabs.tabs('select', title);//选中并刷新
				var currTab = maintabs.tabs('getSelected');
				var url = $(currTab.panel('options').content).attr('src');
				if(url != undefined && currTab.panel('options').title != 'Home') {
					maintabs.tabs('update',{
						tab:currTab,
						options:{
							content:easyui.createFrame(url)
						}
					})
				}
			} else {
				var content = easyui.createFrame(url);
				maintabs.tabs('add',{
					title:title,
					content:content,
					closable:true
				});
			}		
			easyui.tabCloseEven();
		},
		tabCloseEven:function(){
			/*为选项卡绑定右键*/
			$(".tabs-inner").bind('contextmenu',function(e){
				$('#rightMm').menu('show', {
					left: e.pageX,
					top: e.pageY
				});

				var subtitle =$(this).children(".tabs-closable").text();

				$('#rightMm').data("currtab",subtitle);
				$('#maintabs').tabs('select',subtitle);
				return false;
			});
			//刷新
			$('#rightMm-tabupdate').click(function(){
				var currTab = $('#maintabs').tabs('getSelected');
				var url = $(currTab.panel('options').content).attr('src');
				if(url != undefined && currTab.panel('options').title != 'Home') {
					$('#maintabs').tabs('update',{
						tab:currTab,
						options:{
							content:easyui.createFrame(url)
						}
					})
				}
			})
			//关闭当前
			$('#rightMm-tabclose').click(function(){
				var currtab_title = $('#rightMm').data("currtab");
				$('#maintabs').tabs('close',currtab_title);
			})
			//全部关闭
			$('#rightMm-tabcloseall').click(function(){
				$('.tabs-inner span').each(function(i,n){
					var t = $(n).text();
					if(t != 'Home') {
						$('#maintabs').tabs('close',t);
					}
				});
			});
			//关闭除当前之外的TAB
			$('#rightMm-tabcloseother').click(function(){
				var prevall = $('.tabs-selected').prevAll();
				var nextall = $('.tabs-selected').nextAll();		
				if(prevall.length>0){
					prevall.each(function(i,n){
						var t=$('a:eq(0) span',$(n)).text();
						if(t != 'Home') {
							$('#maintabs').tabs('close',t);
						}
					});
				}
				if(nextall.length>0) {
					nextall.each(function(i,n){
						var t=$('a:eq(0) span',$(n)).text();
						if(t != 'Home') {
							$('#maintabs').tabs('close',t);
						}
					});
				}
				return false;
			});
			//关闭当前右侧的TAB
			$('#rightMm-tabcloseright').click(function(){
				var nextall = $('.tabs-selected').nextAll();
				if(nextall.length==0){
					return false;
				}
				nextall.each(function(i,n){
					var t=$('a:eq(0) span',$(n)).text();
					$('#maintabs').tabs('close',t);
				});
				return false;
			});
			//关闭当前左侧的TAB
			$('#rightMm-tabcloseleft').click(function(){
				var prevall = $('.tabs-selected').prevAll();
				if(prevall.length==0){
					return false;
				}
				prevall.each(function(i,n){
					var t=$('a:eq(0) span',$(n)).text();
					$('#maintabs').tabs('close',t);
				});
				return false;
			});

			//退出
			$("#rightMm-exit").click(function(){
				$('#rightMm').menu('hide');
			})
		},
		itembox:function(){
			$(".itembox").each(function(){
				var spanobj=$(this).find("span");
				var len=spanobj.length;
				for(var i=0;i<len;i++){
					if(i+1==len){
						$(spanobj[i]).addClass("icon-joinbottom");
					}else{
						$(spanobj[i]).addClass("icon-join");
					}
				}			
			});
		},
		selectTab:function(){
			$("#maintabs").tabs({
				onSelect:function(title,index){
					$("body").layout("panel","center").panel("setTitle","当前位置："+title);
				}
			});
		},
		createFrame:function(url){
			var s = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:98%;"></iframe>';
			return s;
		},
		change_left_item:function(itemId,name,icon){
			$(".itemline").hide();		
			$("#item_"+itemId).show();
			$("#item_"+itemId).accordion({    
			    animate:true,
			});  
			$(".yukaej_ul li").each(function(i){
				$(this).removeClass("lione");
			});
			$("#menu_"+itemId).addClass("lione");
			$("body").layout("panel","west").panel("setTitle",name,"iconCls",icon);
		},
		showmenu:function(){
			$('#help-menu').menu('show', {
				left: '85%',
				top: 45,
			});
		},
		closeTab:function(){
			var maintabs;
			var currtab_title;
			if($('#maintabs').length>0){
				maintabs=$('#maintabs');
				currtab_title = $('#rightMm').data("currtab");
			}else{
				maintabs=parent.$('#maintabs');	
				currtab_title = parent.$('#rightMm').data("currtab");
			}			
			var tab = maintabs.tabs('getSelected');
			var index = maintabs.tabs('getTabIndex',tab);
			maintabs.tabs('close',index);
		}
	}
})($);
