/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : lcfms

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-10-06 17:08:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for laocheng_admin
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_admin`;
CREATE TABLE `laocheng_admin` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `aname` varchar(20) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `gid` int(100) DEFAULT NULL COMMENT 'groupId',
  `oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_admin
-- ----------------------------
INSERT INTO `laocheng_admin` VALUES ('23', 'admin', 'aa06e8be784bd7b4d30bf21fada8957c', '1', '1');
INSERT INTO `laocheng_admin` VALUES ('24', 'cheng', 'cd824ba4fcffcf5af07714162d41f791', '1', '2');
INSERT INTO `laocheng_admin` VALUES ('25', 'shao', '479716f1bbda05bd8ad52e84f57e0b76', '1', '3');
INSERT INTO `laocheng_admin` VALUES ('26', 'yong', '0160580bfb37d168470844e969a28190', '1', '4');
INSERT INTO `laocheng_admin` VALUES ('27', 'wang', 'aa06e8be784bd7b4d30bf21fada8957c', '1', '4');
INSERT INTO `laocheng_admin` VALUES ('28', 'duan', 'aa06e8be784bd7b4d30bf21fada8957c', '1', '4');

-- ----------------------------
-- Table structure for laocheng_datainfo
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_datainfo`;
CREATE TABLE `laocheng_datainfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table` varchar(50) DEFAULT NULL,
  `outKey` int(11) NOT NULL,
  `realName` varchar(50) DEFAULT NULL,
  `mobile` char(11) DEFAULT NULL,
  `sex` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_datainfo
-- ----------------------------
INSERT INTO `laocheng_datainfo` VALUES ('1', 'laocheng_user', '23', '管理员', null, '0');
INSERT INTO `laocheng_datainfo` VALUES ('2', 'laocheng_user', '24', '成', null, '1');
INSERT INTO `laocheng_datainfo` VALUES ('3', 'laocheng_user', '25', '绍', null, '1');
INSERT INTO `laocheng_datainfo` VALUES ('4', 'laocheng_user', '26', '勇', null, '1');
INSERT INTO `laocheng_datainfo` VALUES ('5', 'laocheng_user', '27', '王', null, '0');
INSERT INTO `laocheng_datainfo` VALUES ('6', 'laocheng_user', '28', '端', null, '0');

-- ----------------------------
-- Table structure for laocheng_demo
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_demo`;
CREATE TABLE `laocheng_demo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s` varchar(50) DEFAULT NULL,
  `i` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_demo
-- ----------------------------
INSERT INTO `laocheng_demo` VALUES ('1', 'aaa', '123');

-- ----------------------------
-- Table structure for laocheng_group
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_group`;
CREATE TABLE `laocheng_group` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `gname` varchar(10) DEFAULT NULL,
  `gdesc` varchar(50) DEFAULT NULL,
  `gpermission` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_group
-- ----------------------------
INSERT INTO `laocheng_group` VALUES ('1', 'admin', '管理员', '33,37,38,39,41');

-- ----------------------------
-- Table structure for laocheng_item
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_item`;
CREATE TABLE `laocheng_item` (
  `itemId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemName` varchar(30) DEFAULT NULL,
  `sort` int(50) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `permitId` varchar(50) DEFAULT '0',
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_item
-- ----------------------------
INSERT INTO `laocheng_item` VALUES ('1', '后台栏目', '0', '0', null, null, '1');
INSERT INTO `laocheng_item` VALUES ('2', '后台框架', '1', '1', null, 'fa-cog', '1');
INSERT INTO `laocheng_item` VALUES ('3', '栏目管理', '3', '2', '', 'fa-bar-chart', '1');
INSERT INTO `laocheng_item` VALUES ('7', '用户管理', '6', '2', '', 'fa-group', '1');
INSERT INTO `laocheng_item` VALUES ('67', '管理栏目', '4', '3', 'admin/item/manage', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('68', '添加栏目', '5', '3', 'admin/item/additem', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('73', '管理用户', '7', '7', 'admin/user/manage_user', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('74', '添加用户', '8', '7', 'admin/user/add_update_view', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('75', '管理用户组', '9', '7', 'admin/user/manage_group', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('76', '添加用户组', '10', '7', 'admin/user/add_group_view', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('81', '标签管理', '11', '2', '', 'fa-wrench', '1');
INSERT INTO `laocheng_item` VALUES ('82', '管理标签', '12', '81', 'admin/tag/manage', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('83', '添加标签', '13', '81', 'admin/tag/addview', 'fa-angle-double-right', '1');
INSERT INTO `laocheng_item` VALUES ('186', '控制台', '2', '2', 'admin/index/first', 'fa-home', '1');

-- ----------------------------
-- Table structure for laocheng_organization
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_organization`;
CREATE TABLE `laocheng_organization` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oname` varchar(50) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_organization
-- ----------------------------
INSERT INTO `laocheng_organization` VALUES ('1', '市场部', null);
INSERT INTO `laocheng_organization` VALUES ('2', '研发部', null);
INSERT INTO `laocheng_organization` VALUES ('3', '行政部', null);
INSERT INTO `laocheng_organization` VALUES ('4', '后勤部', null);

-- ----------------------------
-- Table structure for laocheng_permit_action
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_permit_action`;
CREATE TABLE `laocheng_permit_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `action` varchar(250) DEFAULT NULL,
  `forward` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action` (`action`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_permit_action
-- ----------------------------
INSERT INTO `laocheng_permit_action` VALUES ('41', '系统管理', '标签管理', 'cn.lcfms.app.admin.controller.Tag', 'login.html');
INSERT INTO `laocheng_permit_action` VALUES ('33', '系统管理', '后台框架', 'cn.lcfms.app.admin.controller.Index', 'login.html');
INSERT INTO `laocheng_permit_action` VALUES ('40', 'all', '其他', 'cn.lcfms.app.admin.controller.User.login', 'login.html');
INSERT INTO `laocheng_permit_action` VALUES ('37', '系统管理', '栏目管理', 'cn.lcfms.app.admin.controller.Item', 'login.html');
INSERT INTO `laocheng_permit_action` VALUES ('38', '系统管理', '用户管理', 'cn.lcfms.app.admin.controller.User', 'login.html');
INSERT INTO `laocheng_permit_action` VALUES ('39', '测试', '测试service', 'cn.lcfms.app.index.controller.Test', 'login.html');

-- ----------------------------
-- Table structure for laocheng_process
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process`;
CREATE TABLE `laocheng_process` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `processName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process
-- ----------------------------
INSERT INTO `laocheng_process` VALUES ('1', '用车管理');

-- ----------------------------
-- Table structure for laocheng_process_active
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_active`;
CREATE TABLE `laocheng_process_active` (
  `mid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`mid`,`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_active
-- ----------------------------
INSERT INTO `laocheng_process_active` VALUES ('73', '2', '1');
INSERT INTO `laocheng_process_active` VALUES ('74', '2', '1');
INSERT INTO `laocheng_process_active` VALUES ('74', '4', '1');
INSERT INTO `laocheng_process_active` VALUES ('74', '5', '1');

-- ----------------------------
-- Table structure for laocheng_process_form
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_form`;
CREATE TABLE `laocheng_process_form` (
  `sid` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_form
-- ----------------------------
INSERT INTO `laocheng_process_form` VALUES ('69', 'timeslice', '使用日期', '2017-09-01&douhao;2017-09-26');
INSERT INTO `laocheng_process_form` VALUES ('69', 'text', '使用原因', '的撒撒打算的');
INSERT INTO `laocheng_process_form` VALUES ('69', 'date', '归还日期', '2017-09-29');
INSERT INTO `laocheng_process_form` VALUES ('72', 'select', '是否同意', '拒绝通过');
INSERT INTO `laocheng_process_form` VALUES ('72', 'text', '审核备注', '佛挡杀佛第三方');
INSERT INTO `laocheng_process_form` VALUES ('73', 'timeslice', '使用日期', '2017-09-01&douhao;2017-09-12');
INSERT INTO `laocheng_process_form` VALUES ('73', 'text', '使用原因', '丢失的大佛');
INSERT INTO `laocheng_process_form` VALUES ('73', 'date', '归还日期', '2017-09-29');
INSERT INTO `laocheng_process_form` VALUES ('74', 'select', '是否同意', '同意通过');
INSERT INTO `laocheng_process_form` VALUES ('74', 'text', '审核备注', '发电示范店');
INSERT INTO `laocheng_process_form` VALUES ('75', 'file', '外借登记单', '20170912/1505188084080.png');
INSERT INTO `laocheng_process_form` VALUES ('75', 'file', '外借登记单', '20170912/1505188203723.sql');
INSERT INTO `laocheng_process_form` VALUES ('75', 'text', '登记人', 'fdsfd');
INSERT INTO `laocheng_process_form` VALUES ('76', 'text', '接收人', 'fdfd');

-- ----------------------------
-- Table structure for laocheng_process_manage
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_manage`;
CREATE TABLE `laocheng_process_manage` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `oname` varchar(50) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `uname` varchar(50) DEFAULT NULL,
  `applyTime` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `end` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_manage
-- ----------------------------
INSERT INTO `laocheng_process_manage` VALUES ('73', '1', '1', '市场部', '23', '管理员', '2017-09-11 17:27:40', '1', '失败');
INSERT INTO `laocheng_process_manage` VALUES ('74', '1', '1', '市场部', '23', '管理员', '2017-09-11 17:55:51', '1', '成功');

-- ----------------------------
-- Table structure for laocheng_process_step
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_process_step`;
CREATE TABLE `laocheng_process_step` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `mid` int(11) DEFAULT NULL,
  `stepId` int(11) DEFAULT NULL,
  `stepName` varchar(50) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `oname` varchar(50) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_process_step
-- ----------------------------
INSERT INTO `laocheng_process_step` VALUES ('69', '1', '73', '1', '申请用车', '23', '管理员', '1', '市场部', '2017-09-11 17:27:40');
INSERT INTO `laocheng_process_step` VALUES ('72', '1', '73', '2', '资格审核', '25', '绍', '3', '行政部', '2017-09-11 17:49:27');
INSERT INTO `laocheng_process_step` VALUES ('73', '1', '74', '1', '申请用车', '23', '管理员', '1', '市场部', '2017-09-11 17:55:51');
INSERT INTO `laocheng_process_step` VALUES ('74', '1', '74', '2', '资格审核', '25', '绍', '3', '行政部', '2017-09-11 17:56:02');
INSERT INTO `laocheng_process_step` VALUES ('75', '1', '74', '4', '车辆管理处借出', '26', '勇', '4', '后勤部', '2017-09-12 11:50:08');
INSERT INTO `laocheng_process_step` VALUES ('76', '1', '74', '5', '车辆管理处收回', '26', '勇', '4', '后勤部', '2017-09-12 11:53:18');

-- ----------------------------
-- Table structure for laocheng_tag
-- ----------------------------
DROP TABLE IF EXISTS `laocheng_tag`;
CREATE TABLE `laocheng_tag` (
  `tagId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `tagName` varchar(20) DEFAULT NULL,
  `tagFile` varchar(40) DEFAULT NULL,
  `tempFile` varchar(40) DEFAULT NULL,
  `extraFile` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `download` int(11) DEFAULT '0',
  `ui` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`tagId`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laocheng_tag
-- ----------------------------
INSERT INTO `laocheng_tag` VALUES ('1', '0', '布局', 'fa-map-o', null, null, '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('2', '0', '容器', 'fa-flask', null, null, '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('3', '0', '表单', 'fa-edit', null, null, '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('4', '0', '控件', 'fa-desktop', null, null, '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('6', '1', '120', 'lyrow-120', 'lyrow-12-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('7', '1', '40:80', 'lyrow-40x80', 'lyrow-4x8-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('8', '1', '60:60', 'lyrow-60x60', 'lyrow-6x6-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('9', '1', '80:40', 'lyrow-80x40', 'lyrow-8x4-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('10', '1', '40:40:40', 'lyrow-40x40x40', 'lyrow-4x4x4-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('11', '2', '标题内容', 'conta-title-content', 'conta-title-content-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('12', '2', '选项卡', 'conta-tab', 'conta-tab-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('13', '2', '表单', 'conta-form', 'conta-form-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('15', '4', '数字控件', 'box-number', 'box-number-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('17', '4', '弹出层提示框', 'box-popup', 'box-popup-temp', 'fonts,css/images', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('18', '3', '单行文字', 'box-text', 'box-text-temp', 'fonts', '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('19', '3', '多行文字', 'box-textarea', 'box-textarea-temp', 'fonts', '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('20', '3', '单项选择', 'box-radio', 'box-radio-temp', 'fonts', '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('21', '3', '多项选择', 'box-checkbox', 'box-checkbox-temp', 'fonts', '1', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('22', '3', '下拉选择', 'box-select', 'box-select-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('24', '3', '上传文件', 'box-file', 'box-file-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('26', '2', '手风琴', 'conta-accordion', 'conta-accordion-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('27', '2', '文本段落', 'conta-paragraph', 'conta-paragraph-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('31', '4', '模态窗', 'box-modal', 'box-modal-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('45', '4', '角标徽标', 'box-corner', 'box-corner-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('46', '4', '进度条', 'box-progress', 'box-progress-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('47', '4', '可拖拽滑块', 'box-slider', 'box-slider-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('48', '3', '日历', 'box-kalendae', 'box-kalendae-temp', 'fonts', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('50', '4', '空白行', 'box-blank', 'box-blank-temp', '', '0', '0', 'Bootstrap UI');
INSERT INTO `laocheng_tag` VALUES ('52', '2', '表格', 'conta-table', 'conta-table-temp', '', '0', '0', 'Bootstrap UI');
